local_directory=$(shell pwd)
output_dir=plan_outputs

terraform_validate:
	terragrunt validate --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_validate_assume:
	terragrunt validate -no-color --terragrunt-iam-role 'arn:aws:iam::$(account):role/$(role)' --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_init:
	terragrunt init --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_plan:
	terragrunt plan --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_plan_no_colour:
	terragrunt plan -no-color --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_plan_assume:
	terragrunt plan -no-color --terragrunt-iam-role 'arn:aws:iam::$(account):role/$(role)' --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_apply:
	terragrunt apply -auto-approve --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_apply_assume:
	terragrunt apply -auto-approve -no-color --terragrunt-iam-role 'arn:aws:iam::$(account):role/$(role)' --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate

terraform_destroy:
	terragrunt destroy -auto-approve --terragrunt-config environments/$(environment)/$(platform)/terraform.tfvars --terragrunt-working-dir platform/$(platform)
	rm -r platform/$(platform)/.terraform/terraform.tfstate
