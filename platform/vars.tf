#############
# Variables #
#############
variable "env" {
  type = "string"
}

variable "env_dir" {
  type = "string"
}

variable "overall_tags" {
  type = "map"
}

variable "overall_asg_tags" {
  type    = "list"
  default = []
}

variable "environment_tags" {
  type = "map"
}

variable "environment_asg_tags" {
  type    = "list"
  default = []
}

variable "terraria_account" {
  type    = "map"
  default = {}
}

##################################
# Default vars for resource tags #
##################################
variable "vpc_tags" {
  type    = "map"
  default = {}
}

variable "public_lb_subnet_tags" {
  type = "map"

  default = {
    Zone = "Public"
    Tier = "public-lb"
  }
}

variable "public_lb_route_table_tags" {
  type = "map"

  default = {
    Zone = "Public"
    Tier = "public-lb"
  }
}

variable "public_subnet_tags" {
  type = "map"

  default = {
    Zone = "Public"
    Tier = "public"
  }
}

variable "public_route_table_tags" {
  type = "map"

  default = {
    Zone = "Public"
    Tier = "public"
  }
}

variable "private_lb_subnet_tags" {
  type = "map"

  default = {
    Zone = "Private"
    Tier = "private-lb"
  }
}

variable "private_lb_route_table_tags" {
  type = "map"

  default = {
    Zone = "Private"
    Tier = "private-lb"
  }
}

variable "private_subnet_tags" {
  type = "map"

  default = {
    Zone = "Private"
    Tier = "private"
  }
}

variable "private_route_table_tags" {
  type = "map"

  default = {
    Zone = "Private"
    Tier = "private"
  }
}

variable "restricted_lb_subnet_tags" {
  type = "map"

  default = {
    Zone = "Restricted"
    Tier = "restricted-lb"
  }
}

variable "restricted_lb_route_table_tags" {
  type = "map"

  default = {
    Zone = "Restricted"
    Tier = "restricted-lb"
  }
}

variable "restricted_subnet_tags" {
  type = "map"

  default = {
    Zone = "Restricted"
    Tier = "restricted"
  }
}

variable "restricted_route_table_tags" {
  type = "map"

  default = {
    Zone = "Restricted"
    Tier = "restricted"
  }
}

variable "s3_bucket_tags" {
  type    = "map"
  default = {}
}

variable "kms_key_tags" {
  type    = "map"
  default = {}
}

variable "security_group_tags" {
  type    = "map"
  default = {}
}

variable "emr_cluster_tags" {
  type    = "map"
  default = {}
}

variable "alb_tags" {
  type    = "map"
  default = {}
}

variable "redshift_tags" {
  type    = "map"
  default = {}
}

variable "rds_tags" {
  type    = "map"
  default = {}
}

variable "redis_tags" {
  type    = "map"
  default = {}
}

variable "nacl_tags" {
  type    = "map"
  default = {}
}

variable "sg_tags" {
  type    = "map"
  default = {}
}

variable "vpc_peering_tags" {
  type    = "map"
  default = {}
}

variable "efs_tags" {
  type    = "map"
  default = {}
}

# Note asg_tags must be in the format:
# asg_tags = [
#   {
#     key                 = "key_name"
#     value               = "value_name"
#     propagate_at_launch = true
#   },
# ]

variable "asg_tags" {
  type = "list"

  default = [
    {
      key                 = "Function"
      value               = "Data_Warehouse"
      propagate_at_launch = true
    },
  ]
}

variable "secretsmanager_tags" {
  type    = "map"
  default = {}
}
