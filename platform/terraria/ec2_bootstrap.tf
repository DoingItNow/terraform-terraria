resource "aws_security_group" "temp_jenkins_sg" {
  provider = "aws.terraria"

  name        = "application-${lookup(var.environment_tags, "Environment")}-ec2-bootstrap-sg"
  description = "Provides a base level access for the platform"
  vpc_id      = "${data.aws_vpc.application_vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    description = "Outbound access to internet"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH Access"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 7777
    to_port     = 7777
    protocol    = "udp"
    description = "Terraria Server UDP inbound"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 7777
    to_port     = 7777
    protocol    = "tcp"
    description = "Terraria Server TCP inbound"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.overall_tags, var.environment_tags, var.sg_tags, map("Name", format("%s", "temp-jenkins-${lookup(var.environment_tags, "Environment")}-base-sg")))}"
}

resource "aws_iam_instance_profile" "bootstrap_profile" {
  provider = "aws.terraria"
  name     = "bootstrap_profile"
  role     = "${aws_iam_role.bootstrap_role.name}"
}

resource "aws_iam_role" "bootstrap_role" {
  provider = "aws.terraria"
  name     = "bootstrap_role"
  path     = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "bootstrap_policy" {
  provider    = "aws.terraria"
  name        = "test-policy"
  description = "A test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ] 
}
EOF
}

resource "aws_iam_role_policy_attachment" "bootstrap_attach" {
  provider   = "aws.terraria"
  role       = "${aws_iam_role.bootstrap_role.name}"
  policy_arn = "${aws_iam_policy.bootstrap_policy.arn}"
}

module "temp_jenkins_ec2_server" {
  source = "../../modules/compute/ec2"

  providers {
    aws = "aws.terraria"
  }

  name           = "${lookup(var.terraria_account, "ec2.app.ec2_name")}"
  instance_count = "${lookup(var.terraria_account, "ec2.app.instance_count")}"

  ami                         = "ami-07a3bd4944eb120a0"
  instance_type               = "${lookup(var.terraria_account, "ec2.app.instance_type")}"
  key_name                    = "${lookup(var.terraria_account, "ec2.app.key_name")}"
  monitoring                  = "${lookup(var.terraria_account, "ec2.app.monitoring", true)}"
  associate_public_ip_address = "${lookup(var.terraria_account, "ec2.app.associate_public_ip_address", false)}"
  vpc_security_group_ids      = ["${aws_security_group.temp_jenkins_sg.id}"]
  subnet_id                   = "${data.aws_subnet.app_private_subnet.id}"
  user_data                   = "${data.template_file.ec2_userdata.rendered}"
  iam_instance_profile        = "${aws_iam_instance_profile.bootstrap_profile.name}"
  ec2_tags                    = "${merge(var.overall_tags, var.environment_tags)}"
}
