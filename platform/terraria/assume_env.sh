#!/bin/bash

## This is a script that will output the credentials needed to assume the role to deploy terraform scripts

ACCOUNT_ID="${1}"
ROLE="${2}"
AWS_ARN="arn:aws:iam::${ACCOUNT_ID}:role/${ROLE}"

# Test to see if jq is installed
jq 1>/dev/null 2>&1
if [ $? -eq 127 ]; then
    echo 'ERROR: jq package not found'
    exit 1
fi

# Jenkins Deployer Role
STS_OUTPUT=$(aws sts assume-role --role-arn "${AWS_ARN}" --role-session-name 'PackerSession')

export AWS_ACCESS_KEY_ID=$(echo "${STS_OUTPUT}" | jq -r '.Credentials.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo "${STS_OUTPUT}" | jq -r '.Credentials.SecretAccessKey')
export AWS_SESSION_TOKEN=$(echo "${STS_OUTPUT}" | jq -r '.Credentials.SessionToken')

echo "Add these env variables into current terminal session"
echo "export AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID
echo "export AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY
echo "export AWS_SESSION_TOKEN="$AWS_SESSION_TOKEN
