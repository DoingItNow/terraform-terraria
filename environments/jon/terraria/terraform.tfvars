terragrunt = {
  include {
    path = "${find_in_parent_folders()}"
  }

  terraform {
    extra_arguments "custom_vars" {
      commands = ["${get_terraform_commands_that_need_vars()}"]

      arguments = [
        "-var-file=${get_tfvars_dir()}/../../terraform.tfvars",
        "-var-file=${get_tfvars_dir()}/terraform.tfvars",
        "-var",
        "env=jon",
        "-var",
        "env_dir=${get_tfvars_dir()}/../../common/terraria",
      ]
    }
  }
}

environment_tags = {
  Environment = "jon"
}

terraria_account = {
  deployer_role_arn = "arn:aws:iam::677635131472:role/jenkins-deployer-role"
  dns_role_arn      = "arn:aws:iam::677635131472:role/jenkins-deployer-role"

  ec2.app.ec2_name                    = "terraria-server"
  ec2.app.instance_count              = "1"
  ec2.app.instance_type               = "t2.medium"
  ec2.app.key_name                    = "jon-contino"
  ec2.app.monitoring                  = true
  ec2.app.ami_name                    = "amzn2-ami-hvm-*-x86_64-gp2"
  ec2.app.userdata_script             = "userdata.sh"
  ec2.app.associate_public_ip_address = true
}
