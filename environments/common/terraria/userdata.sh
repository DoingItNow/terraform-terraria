#!/bin/bash

cd /home/ubuntu

sudo apt-get update
sudo apt-get install unzip -y
sudo apt-get install mono-complete -y
sudo apt-get install awscli -y
wget https://github.com/Pryaxis/TShock/releases/download/v4.3.25/tshock_4.3.25.zip
unzip tshock_4.3.25.zip
chmod +x TerrariaServer.exe


aws s3 cp "s3://terraria-world-bucket/Guangstralia.wld" "Guangstralia.wld"
aws s3 cp "s3://terraria-world-bucket/Guangstralia.wld.bak" "Guangstralia.wld.bak"
aws s3 cp "s3://terraria-world-bucket/start_server.sh" "start_server.sh"
chmod +x start_server.sh

sudo touch copy-terraria-files
cat > copy-terraria-files <<EOF
#!/bin/bash
aws s3 cp "/home/ubuntu/Guangstralia.wld" "s3://terraria-world-bucket/Guangstralia.wld"
aws s3 cp "/home/ubuntu/Guangstralia.wld.bak" "s3://terraria-world-bucket/Guangstralia.wld.bak"
EOF
chmod +x copy-terraria-files
sudo mv copy-terraria-files /etc/cron.hourly/copy-terraria-files

