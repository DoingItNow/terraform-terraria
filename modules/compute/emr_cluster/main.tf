# EMR IAM resources
data "aws_iam_policy_document" "emr_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["elasticmapreduce.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "emr_service_role" {
  name               = "${var.service_role_name}"
  assume_role_policy = "${data.aws_iam_policy_document.emr_assume_role.json}"
}

resource "aws_iam_role_policy_attachment" "emr_service_role" {
  role       = "${aws_iam_role.emr_service_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceRole"
}

resource "aws_iam_policy" "emr_service_role_policy" {
  name   = "${var.service_role_policy_name}"
  policy = "${var.service_role_policy_document}"
}

resource "aws_iam_role_policy_attachment" "emr_service_role_policy" {
  role       = "${aws_iam_role.emr_service_role.name}"
  policy_arn = "${aws_iam_policy.emr_service_role_policy.arn}"
}

# EMR IAM resources for EC2
data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "emr_ec2_instance_profile" {
  name               = "${var.instance_profile_name}"
  assume_role_policy = "${data.aws_iam_policy_document.ec2_assume_role.json}"
}

resource "aws_iam_policy" "emr_ec2_instance_policy" {
  name   = "${var.instance_profile_policy_name}"
  policy = "${var.instance_profile_policy_document}"
}

resource "aws_iam_role_policy_attachment" "emr_ec2_instance_profile_custom" {
  role       = "${aws_iam_role.emr_ec2_instance_profile.name}"
  policy_arn = "${aws_iam_policy.emr_ec2_instance_policy.arn}"
}

data "aws_region" "current" {}

data "aws_iam_policy_document" "emr_for_ec2_role_policy" {
  statement {
    sid    = "AmazonLinuxAMIRepositoryAccess"
    effect = "Allow"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::packages.*.amazonaws.com/*",
      "arn:aws:s3:::repo.*.amazonaws.com/*",
    ]
  }

  statement {
    sid    = "AccessToEMRLogBucketsForSupport"
    effect = "Allow"

    actions = [
      "s3:Put*",
      "s3:Get*",
      "s3:Create*",
      "s3:Abort*",
      "s3:List*",
    ]

    resources = [
      "arn:aws:s3:::aws157-logs-prod-${data.aws_region.current.name}/*",
      "arn:aws:s3:::aws157-logs-prod/*",
    ]
  }

  statement {
    sid    = "StaticAmazonEMRforEC2RoleWithNoS3"
    effect = "Allow"

    actions = [
      "cloudwatch:*",
      "dynamodb:*",
      "ec2:Describe*",
      "elasticmapreduce:Describe*",
      "elasticmapreduce:ListBootstrapActions",
      "elasticmapreduce:ListClusters",
      "elasticmapreduce:ListInstanceGroups",
      "elasticmapreduce:ListInstances",
      "elasticmapreduce:ListSteps",
      "rds:Describe*",
      "sdb:*",
      "sns:*",
      "sqs:*",
      "glue:CreateDatabase",
      "glue:UpdateDatabase",
      "glue:DeleteDatabase",
      "glue:GetDatabase",
      "glue:GetDatabases",
      "glue:CreateTable",
      "glue:UpdateTable",
      "glue:DeleteTable",
      "glue:GetTable",
      "glue:GetTables",
      "glue:GetTableVersions",
      "glue:CreatePartition",
      "glue:BatchCreatePartition",
      "glue:UpdatePartition",
      "glue:DeletePartition",
      "glue:BatchDeletePartition",
      "glue:GetPartition",
      "glue:GetPartitions",
      "glue:BatchGetPartition",
      "glue:CreateUserDefinedFunction",
      "glue:UpdateUserDefinedFunction",
      "glue:DeleteUserDefinedFunction",
      "glue:GetUserDefinedFunction",
      "glue:GetUserDefinedFunctions",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "emr_for_ec2_role_policy" {
  name   = "${var.instance_profile_managed_policy_name}"
  policy = "${data.aws_iam_policy_document.emr_for_ec2_role_policy.json}"
}

resource "aws_iam_role_policy_attachment" "emr_ec2_instance_profile_emr" {
  role       = "${aws_iam_role.emr_ec2_instance_profile.name}"
  policy_arn = "${aws_iam_policy.emr_for_ec2_role_policy.arn}"
}

resource "aws_iam_instance_profile" "emr_ec2_instance_profile" {
  name = "${aws_iam_role.emr_ec2_instance_profile.name}"
  role = "${aws_iam_role.emr_ec2_instance_profile.name}"
}

# Security group resources
resource "aws_security_group" "emr_master_managed" {
  name                   = "${var.cluster_name}-master-managed-sg"
  description            = "AWS managed EMR security group"
  vpc_id                 = "${var.vpc_id}"
  revoke_rules_on_delete = true

  tags = "${var.master_security_group_tags}"
}

resource "aws_security_group" "emr_master_access" {
  name        = "${var.cluster_name}-master-access-sg"
  description = "Provides extra access to the EMR cluster"
  vpc_id      = "${var.vpc_id}"

  ingress {
    description     = "Bastion to SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to YARN ResourceManager"
    from_port       = 8088
    to_port         = 8088
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to Hadoop HDFS NameNode"
    from_port       = 50070
    to_port         = 50070
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to Spark HistoryServer"
    from_port       = 18080
    to_port         = 18080
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to Hue"
    from_port       = 8888
    to_port         = 8888
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to ThriftServer"
    from_port       = 10001
    to_port         = 10001
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to Ganglia"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  egress {
    description = "LDAP to SS"
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = ["${var.shared_services_cidr}"]
  }

  egress {
    description = "LDAPS to SS"
    from_port   = 636
    to_port     = 636
    protocol    = "tcp"
    cidr_blocks = ["${var.shared_services_cidr}"]
  }

  egress {
    description = "Everything to Anything (S3 + Bootstrap)"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${var.master_security_group_tags}"
}

resource "aws_security_group" "emr_slave_managed" {
  name                   = "${var.cluster_name}-slave-managed-sg"
  description            = "AWS managed EMR security group"
  vpc_id                 = "${var.vpc_id}"
  revoke_rules_on_delete = true

  tags = "${var.slave_security_group_tags}"
}

resource "aws_security_group" "emr_slave_access" {
  name        = "${var.cluster_name}-slave-access-sg"
  description = "Provides extra access to the EMR cluster"
  vpc_id      = "${var.vpc_id}"

  ingress {
    description     = "Bastion to SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to YARN NodeManager"
    from_port       = 8042
    to_port         = 8042
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  ingress {
    description     = "Bastion to Hadoop HDFS DataNode"
    from_port       = 50075
    to_port         = 50075
    protocol        = "tcp"
    security_groups = ["${var.bastion_access_sg}"]
  }

  egress {
    description = "Everything to Anything (S3 + Bootstrap)"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${var.slave_security_group_tags}"
}

resource "aws_security_group" "emr_service_access" {
  name                   = "${var.cluster_name}-service-access-sg"
  vpc_id                 = "${var.vpc_id}"
  revoke_rules_on_delete = true

  tags = "${var.service_access_security_group_tags}"
}

resource "aws_emr_security_configuration" "emr_security_config" {
  name = "emr_security_config"

  configuration = <<EOF
{
  "EncryptionConfiguration": {
		"EnableInTransitEncryption" : true,
		"EnableAtRestEncryption" : false,
		"InTransitEncryptionConfiguration" : {
			"TLSCertificateConfiguration" : {
				"S3Object" : "${var.s3_cert_object}",
				"CertificateProviderType" : "PEM"
			}
		}
 	}
}
EOF
}

# EMR resources
resource "aws_emr_cluster" "cluster" {
  name           = "${var.cluster_name}"
  release_label  = "${var.release_label}"
  applications   = "${var.applications}"
  configurations = "${var.configurations}"

  ec2_attributes {
    key_name                          = "${var.key_name}"
    subnet_id                         = "${var.subnet_id}"
    emr_managed_master_security_group = "${aws_security_group.emr_master_managed.id}"
    emr_managed_slave_security_group  = "${aws_security_group.emr_slave_managed.id}"
    instance_profile                  = "${aws_iam_instance_profile.emr_ec2_instance_profile.arn}"
    service_access_security_group     = "${aws_security_group.emr_service_access.id}"
    additional_master_security_groups = "${aws_security_group.emr_master_access.id}"
    additional_slave_security_groups  = "${aws_security_group.emr_slave_access.id}"
  }

  instance_group = "${var.instance_groups}"

  ebs_root_volume_size = "${var.ebs_root_volume_size}"

  bootstrap_action {
    name = "${var.bootstrap_name}"
    path = "${var.bootstrap_uri}"
    args = "${var.bootstrap_args}"
  }

  step {
    name              = "${var.post_bootstrap_step_name}"
    action_on_failure = "TERMINATE_CLUSTER"

    hadoop_jar_step {
      jar  = "${var.post_bootstrap_step_jar}"
      args = ["${var.post_bootstrap_step_args}"]
    }
  }

  security_configuration = "${aws_emr_security_configuration.emr_security_config.name}"

  log_uri      = "${var.log_uri}"
  service_role = "${aws_iam_role.emr_service_role.arn}"
  tags         = "${var.emr_cluster_tags}"
}


resource "aws_cloudwatch_event_rule" "emr_rule_terminated_with_errors" {
  name        = "capture-emr-terminated-with-errors"
  description = "Capture EMR Alert when cluster terminates with errors"

  event_pattern = <<PATTERN
{
  "source": [
    "aws.emr"
  ],
  "detail-type": [
    "EMR Cluster State Change"
  ],
  "detail": {
    "state": [
      "TERMINATED_WITH_ERRORS"
    ]
  }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "emr_rule_terminated_with_errors_target" {
  rule      = "${aws_cloudwatch_event_rule.emr_rule_terminated_with_errors.name}"
  arn       = "${var.emr_sns_alert_topic_arn}"
}

resource "aws_cloudwatch_event_rule" "emr_rule_terminated_with_failures" {
  name        = "capture-emr-terminated-with-failures"
  description = "Capture EMR Alert when cluster terminates with failures"

  event_pattern = <<PATTERN
{
  "source": [
    "aws.emr"
  ],
  "detail-type": [
    "EMR Cluster State Change"
  ],
  "detail": {
    "stateChangeReason": {
      "code": [
        "INTERNAL_ERROR",
        "VALIDATION_ERROR",
        "INSTANCE_FAILURE",
        "BOOTSTRAP_FAILURE",
        "STEP_FAILURE"
      ],
      "message": [
        "*"
      ]
    },
    "state": [
      "TERMINATED"
    ]
  }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "emr_rule_terminated_with_failures_target" {
  rule      = "${aws_cloudwatch_event_rule.emr_rule_terminated_with_failures.name}"
  arn       = "${var.emr_sns_alert_topic_arn}"
}

resource "aws_cloudwatch_event_rule" "emr_rule_instance_group_errors" {
  name        = "capture-emr-instance-group-errors"
  description = "Capture EMR Alert when an Instance Group has errors"

  event_pattern = <<PATTERN
{
  "source": [
    "aws.emr"
  ],
  "detail-type": [
    "EMR Instance Group State Change"
  ],
  "detail": {
    "state": [
      "ARRESTED"
    ]
  }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "emr_rule_instance_group_errors_target" {
  rule      = "${aws_cloudwatch_event_rule.emr_rule_instance_group_errors.name}"
  arn       = "${var.emr_sns_alert_topic_arn}"
}
