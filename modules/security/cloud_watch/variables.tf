variable "logging_group_name" {
  description = "Name of the CloudWatch loggroup"
}

variable "alerting_email" {
  description = "Email used for alerting"
  default     = "peter.lawson@medibank.com.au"
}
