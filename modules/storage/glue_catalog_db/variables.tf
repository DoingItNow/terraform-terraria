variable "name" {
  description = "The name of the database"
  type        = "string"
}

variable "description" {
  description = "Description of the database"
  type        = "string"
  default     = ""
}

variable "location_uri" {
  description = "The location of the database (for example, an HDFS path)"
  type        = "string"
  default     = ""
}

variable "parameters" {
  description = "A map that defines parameters and properties of the database"
  type        = "map"
  default     = {}
}
