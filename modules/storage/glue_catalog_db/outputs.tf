output "name" {
  description = "The name of the database"
  value       = "${aws_glue_catalog_database.aws_glue_catalog_database.name}"
}
