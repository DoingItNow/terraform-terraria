data "aws_iam_policy_document" "policy" {
  source_json = "${var.bucket_policy}"

  statement {
    sid = "ensure-private-read-write"

    actions = [
      "s3:PutObject",
      "s3:PutObjectAcl",
    ]

    effect = "Deny"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"

      values = [
        "public-read",
        "public-read-write",
      ]
    }
  }
}

resource "aws_s3_bucket" "private_bucket" {
  bucket = "${var.bucket_name}"
  acl    = "private"
  policy = "${data.aws_iam_policy_document.policy.json}"
  tags   = "${var.bucket_tags}"

  versioning {
    enabled = true
  }

  acl = "log-delivery-write"

  lifecycle_rule {
    enabled = true

    abort_incomplete_multipart_upload_days = "${var.abort_incomplete_multipart_upload_days}"

    expiration {
      expired_object_delete_marker = true
    }

    noncurrent_version_transition {
      days          = "${var.noncurrent_ia_transition_days}"
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_expiration {
      days = "${var.noncurrent_version_expiration_days}"
    }

    transition {
      days          = "${var.current_ia_transition_days}"
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = "${var.current_glacier_transition_days}"
      storage_class = "GLACIER"
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
