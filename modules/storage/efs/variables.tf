variable "creation_token" {
  description = "Name of the EFS resource"
  type        = "string"
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key"
  type        = "string"
  default     = ""
}

variable "efs_tags" {
  description = "A mapping of tags to assign to the file system"
  type        = "map"
  default     = {}
}

variable "subnet_ids" {
  description = "A list of the subnet ids to add the mount target in."
  type        = "list"
  default     = []
}

variable "encrypted" {
  description = "If true, the disk will be encrypted."
  default     = true
}


variable "security_groups" {
  description = "A list of the subnets to add to the EFS secuirty group."
  type        = "list"
  default     = []
}



