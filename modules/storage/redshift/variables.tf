variable "cluster_identifier" {
  description = "Name of the cluster"
}

variable "cluster_version" {
  description = "Version of Redshift engine cluster"
  default     = "1.0"

  # Constraints: Only version 1.0 is currently available.
  # http://docs.aws.amazon.com/cli/latest/reference/redshift/create-cluster.html
}

variable "cluster_node_type" {
  description = "Node Type of Redshift cluster"

  # Valid Values: dc2.large | dc2.8xlarge | ds2.xlarge | ds2.8xlarge.
  # http://docs.aws.amazon.com/cli/latest/reference/redshift/create-cluster.html
}

variable "cluster_number_of_nodes" {
  description = "Number of nodes in the cluster (values greater than 1 will trigger 'cluster_type' of 'multi-node')"
  default     = 3
}

variable "cluster_database_name" {
  description = "The name of the database to create"
}

variable "cluster_master_username" {
  description = "Set the admin users username"
}

variable "cluster_master_password" {
  description = "Set the admin users password"
}

variable "cluster_port" {
  description = "Redshift port"
  default     = 5439
}

variable "cluster_parameter_group" {
  description = "Parameter group, depends on DB engine used"
  default     = "redshift-1.0"
}

variable "cluster_iam_roles" {
  description = "A list of IAM Role ARNs to associate with the cluster. A Maximum of 10 can be associated to the cluster at any time."
  type        = "list"
  default     = []
}

variable "access_policy_name" {
  description = "Cluster access policy name"
}

variable "access_policy_document" {
  description = "Cluster access policy document"
}

variable "access_role_name" {
  description = "Cluster access role name"
}

variable "redshift_subnet_group_name" {
  description = "The name of a cluster subnet group to be associated with this cluster. If not specified, new subnet will be created."
  default     = ""
}

variable "parameter_group_name" {
  description = "The name of the parameter group to be associated with this cluster. If not specified new parameter group will be created."
  default     = ""
}

variable "subnets" {
  description = "List of subnets DB should be available at. It might be one subnet."
  default     = []
}

variable "vpc_security_group_ids" {
  description = "A list of Virtual Private Cloud (VPC) security groups to be associated with the cluster."
  default     = []
}

variable "final_snapshot_identifier" {
  description = "(Optional) The identifier of the final snapshot that is to be created immediately before deleting the cluster. If this parameter is provided, 'skip_final_snapshot' must be false."
  default     = false
}

variable "skip_final_snapshot" {
  description = "If true (default), no snapshot will be made before deleting DB"
  default     = true
}

variable "preferred_maintenance_window" {
  description = "When AWS can run snapshot, can't overlap with maintenance window"
  default     = "sat:10:00-sat:10:30"
}

variable "automated_snapshot_retention_period" {
  description = "How long will we retain backups"
  default     = 0
}

variable "logging_bucket_name" {
  description = "The name of an existing S3 bucket where the log files are to be stored. Must be in the same region as the cluster and the cluster must have read bucket and put object permissions."
}

variable "logging_s3_key_prefix" {
  description = "(Optional) The prefix applied to the log file names."
  default     = ""
}

variable "wlm_json_configuration" {
  default = "[{\"query_concurrency\": 5}]"
}

variable "tags" {
  description = "A mapping of tags to assign to all resources"
  default     = {}
}

variable "kms_key_id" {
  description = "The ARN for the KMS encryption key. When specifying kms_key_id, encrypted needs to be set to true."
}

variable "allow_version_upgrade" {
  description = "(Optional) If true, major version upgrades can be applied during the maintenance window to the Amazon Redshift engine that is running on the cluster."
  default     = true
}
