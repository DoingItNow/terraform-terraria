variable "nat_gateway_name" {
  description = "Name to be used on the nat gateway name tag"
  default     = ""
}
variable "vpc_tags" {
  description = "Tags for the vpc"
  default     = {}
}
variable "public_subnet_ids" {
  description = "List of IDs of public subnets"
  default     = []
}

variable "private_route_table_ids" {
  description = "A list of nat gateway IDs inside the VPC"
  default     = []
}

variable "azs" {
  description = "A list of availability zones in the region"
  default     = []
}

variable "nat_gateway_count" {
  description = "The number of nat gateways for the VPC"
  default     = 1
}

variable "reuse_nat_ips" {
  description = "Should be true if you don't want EIPs to be created for your NAT Gateways and will instead pass them in via the 'external_nat_ip_ids' variable"
  default     = false
}

variable "external_nat_ip_ids" {
  description = "List of EIP IDs to be assigned to the NAT Gateways (used in combination with reuse_nat_ips)"
  type        = "list"
  default     = []
}

variable "nat_gateway_tags" {
  description = "Additional tags for the NAT gateways"
  default     = {}
}

variable "nat_eip_tags" {
  description = "Additional tags for the NAT EIP"
  default     = {}
}

variable "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  default     = false
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  default     = false
}
