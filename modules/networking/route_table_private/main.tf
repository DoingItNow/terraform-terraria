locals {
  max_subnet_length = "${max(length(var.subnets))}"

  #   max_private_subnet_length = "${max(length(var.private_subnets),length(var.database_subnets))}"

  # Determine the amount of nat gateways needed. It can be either 1, 3 (3 AZs) or number of private subnets
  nat_gateway_count = "${var.single_nat_gateway ? 1 : (var.one_nat_gateway_per_az ? length(var.azs) : local.max_subnet_length)}"

  # If nat gateways are enabled, get the nat gateway count as the number of route tables otherwise have only one route table
  route_table_count = "${local.max_subnet_length > 0 ? (var.enable_nat_gateway ? local.nat_gateway_count : 1) : 0}"
}

resource "aws_route_table" "private" {
  count = "${local.route_table_count}"

  vpc_id = "${var.vpc_id}"

  tags = "${merge(map("Name", format("%s-%s-route%s", var.route_table_name, var.network_tier, local.route_table_count > 1 ? format("-%s", element(var.azs, count.index)) : "")), var.route_table_tags, var.vpc_tags)}"

  lifecycle {
    # When attaching VPN gateways it is common to define aws_vpn_gateway_route_propagation
    # resources that manipulate the attributes of the routing table (typically for the private subnets)
    ignore_changes = ["propagating_vgws"]
  }
}

resource "aws_route_table_association" "private" {
  count = "${length(var.subnets) > 0 ? length(var.subnets) : 0}"

  subnet_id      = "${element(var.subnet_ids, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, (var.single_nat_gateway ? 0 : count.index))}"
}
