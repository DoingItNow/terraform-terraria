resource "aws_route_table" "public" {
  count  = "${length(var.subnets) > 0 ? 1 : 0}"
  vpc_id = "${var.vpc_id}"

  tags = "${merge(map("Name", format("%s-%s-route", var.route_table_name, var.network_tier)), var.route_table_tags, var.vpc_tags)}"
}

resource "aws_route_table_association" "public" {
  count = "${length(var.subnets) > 0 ? length(var.subnets) : 0}"

  subnet_id      = "${element(var.subnet_ids, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}
