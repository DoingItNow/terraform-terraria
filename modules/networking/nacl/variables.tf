variable "vpc_id" {
  description = "The VPC ID"
}

variable "subnet_ids" {
  description = "A list of subnets that the NACL is associated with"
  default     = []
}

variable "nacl_name" {
  description = "The NACL name"
  default     = ""
}

variable "ingress_with_cidr_blocks" {
  description = "List of ingress rules to create where 'cidr_blocks' is used"
  default     = []
}

variable "egress_with_cidr_blocks" {
  description = "List of egress rules to create where 'cidr_blocks' is used"
  default     = []
}

variable "nacl_tags" {
  description = "A map of tags"
  default     = {}
}
