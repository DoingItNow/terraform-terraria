output "vpc_peering_connection_id" {
  description = "The ID of the VPC Peering connection"
  value       = "${element(concat(aws_vpc_peering_connection.current_vpc.*.id, list("")), 0)}"
}
