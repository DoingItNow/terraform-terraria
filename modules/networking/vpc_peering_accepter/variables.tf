variable "local_name" {
  description = "The remote side that initiated the request"
}

variable "peer_name" {
  description = "This account that's accepting the request"
}

variable "vpc_peering_connection_id" {
  description = "The vpc peering connection ID"
}

variable "vpc_peering_tags" {
  description = "Tags for the vpc peering"
  default     = {}
}
