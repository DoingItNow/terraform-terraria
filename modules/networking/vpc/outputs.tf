output "vpc_id" {
  description = "The ID of the VPC"
  value       = "${element(concat(aws_vpc.new_vpc.*.id, list("")), 0)}"
}

output "vpc_name" {
  description = "The name of the VPC"
  value       = "${var.vpc_name}"
}

output "vpc_tags" {
  description = "The VPC tags"
  value       = "${var.vpc_tags}"
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = "${element(concat(aws_vpc.new_vpc.*.cidr_block, list("")), 0)}"
}

output "vpc_flow_logs_log_group" {
  description = "The name of the VPC Flow Logs, Log Group"
  value       = "${aws_cloudwatch_log_group.flow_log_group.name}"
}
