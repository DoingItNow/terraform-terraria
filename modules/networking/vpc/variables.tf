variable "vpc_name" {
  description = "Name to be used on all the resources as identifier"
}

variable "cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overriden"
  default     = "0.0.0.0/0"
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}

variable "vpc_tags" {
  description = "Additional tags for the VPC"
  default     = {}
}

variable "cloudwatch_log_group_tags" {
  description = "Additional tags for the VPC Flow Logs Log Group"
  default     = {}
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  default     = false
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  default     = true
}

variable "ntp_servers" {
  description = "This is the NTP servers for the VPC"
  default     = []
}

variable "domain_name" {
  description = "This is the Domain Name for the VPC"
  default     = ""
}

variable "domain_name_servers" {
  description = "This is the Domain Name servers for the VPC"
  default     = []
}

variable "dhcp_enabled" {
  description = "Enable dhcp options"
  default = true
}
