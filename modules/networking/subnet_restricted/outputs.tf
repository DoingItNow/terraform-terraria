output "subnet_ids" {
  description = "List of IDs of restricted subnets"
  value       = ["${aws_subnet.restricted.*.id}"]
}

output "subnets_list" {
  description = "List of restricted subnets"
  value       = "${var.subnets}"
}

output "subnets_cidr_blocks" {
  description = "List of cidr blocks of restricted subnets"
  value       = ["${aws_subnet.restricted.*.cidr_block}"]
}

output "azs" {
  description = "List Availability Zones"
  value       = "${var.azs}"
}
