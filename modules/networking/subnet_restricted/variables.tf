variable "vpc_id" {
  description = "The VPC ID"
}

variable "subnet_name" {
  description = "Name to be used on all the resources as identifier"
}

variable "network_tier" {
  description = "The network tier where the subnet is placed"
}

variable "create_subnet_database_group" {
  description = "Controls if restricted subnet database_group should be created"
  default     = true
}

variable "subnets" {
  description = "A list of restricted subnets inside the VPC"
  default     = []
}

variable "subnet_group_tags" {
  description = "Additional tags for the restricted subnets"
  default     = {}
}

variable "azs" {
  description = "A list of availability zones in the region"
  default     = []
}

variable "vpc_tags" {
  description = "Tags for the vpc"
  default     = {}
}
