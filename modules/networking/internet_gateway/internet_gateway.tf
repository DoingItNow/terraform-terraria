resource "aws_internet_gateway" "igw" {
  count = "${length(var.public_subnets) > 0 ? 1 : 0}"

  vpc_id = "${var.vpc_id}"

  tags = "${merge(map("Name", format("%s-igw", var.igw_name)), var.igw_tags, var.vpc_tags)}"
}

resource "aws_route" "public_internet_gateway" {
  count = "${length(var.public_subnets) > 0 ? 1 : 0}"

  route_table_id         = "${var.public_route_table_id[0]}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.igw.id}"

  timeouts {
    create = "5m"
  }
}
